# Java LDAP Tool

This application is a simple Java LDAP Client that can check the LDAP server of itd.umich.edu.<br />
For Computer Networks 2 course of Avans Informatica P2.4.

## Setup

The min. required JDK and JavaFX is 14.0 in order to run the application.

If you haven't installed Java FX yet, Go to this website and follow the
'JavaFX and Intellij' tutorial:

https://openjfx.io/openjfx-docs/#install-java