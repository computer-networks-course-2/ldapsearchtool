package GUI;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import javafx.scene.input.MouseEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Start {

    private final String[] connections = new String[]{"ldap://ldap.itd.umich.edu"};

    private static String choiceError = "Choose only one option!";
    private static String invalidConnectionString = "Connection string is not valid!";
    private static String connectionError = "Connection not possible";
    private Pattern ldapPattern;
    private String selectedConnection;

    public Button connectionBtn;
    public ChoiceBox<String> connectionChoiceBox;
    public TextField connectionTextField;
    public Label errorMessage;

    public Start() {
        ldapPattern = Pattern
                .compile("^(ldap://|ldaps://)((\\\\d{1,3}\\\\.){3}\\\\d{1,3}(:\\\\d{1,5})?)$");
        selectedConnection = "";
    }

    // Check if connection choice is valid
    private boolean choiceIsValid() {
        if (!connectionChoiceBox.getSelectionModel().isEmpty() && connectionTextField.getText().isEmpty() ||
                connectionChoiceBox.getSelectionModel().isEmpty() && !connectionTextField.getText().isEmpty()) {

            if (!connectionChoiceBox.getValue().isEmpty() && connectionTextField.getText().isEmpty()) {
                selectedConnection = connectionChoiceBox.getSelectionModel().getSelectedItem();
                return true;

            } else if (connectionChoiceBox.getValue().isEmpty() && !connectionTextField.getText().isEmpty()) {
                Matcher m = ldapPattern.matcher(connectionTextField.getText());

                if (m.matches()) {
                    selectedConnection = connectionTextField.getText();
                    return true;
                }
                errorMessage.setText(invalidConnectionString);
                selectedConnection = "";
                return false;

            } else {
                errorMessage.setText(choiceError);
                selectedConnection = "";
                return false;
            }
        }

        selectedConnection = "";
        errorMessage.setText("No choice has been made!");
        return false;
    }

    private void initConnectionChoiceBox() {
        connectionChoiceBox.getItems().addAll(connections);
    }

    @FXML
    public void initialize() {
        initConnectionChoiceBox();

        connectionBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (choiceIsValid()) {
                System.out.println(selectedConnection);
                LDAPApplication.setLDAPServer(selectedConnection);
                LDAPApplication.initNewLDAPContext();
            }
        });
    }
}
