package GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Hashtable;

public class LDAPApplication extends Application {

    private static ScreenState state;
    private static Stage window;

    public static DirContext LDAPContext;


    // LDAP Server target
    public static String LDAPServer = "";

    static void setState(ScreenState s) {
        if (s != null && s != state) {
            state = s;

            try {
                FXMLLoader loader = new FXMLLoader();

                Scene scene = new Scene(loader.load(new FileInputStream(s.getFile())));

                window.setScene(scene);

                window.show();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void start(Stage s) {
        window = s;

        window.setResizable(false);
        window.requestFocus();

        window.setTitle("LDAP Search Client");

        setState(ScreenState.START);
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static void initNewLDAPContext() {
        // Setup environment for creating the initial context
        // Anonymous access thus authentication setup is unnecessary
        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, LDAPServer);

        try {
            // Create initial context
            LDAPContext = new InitialDirContext(env);
            setState(ScreenState.DASHBOARD);

        } catch (NamingException ne) {
            ne.printStackTrace();
        }
    }

    public static String getLDAPServer() {
        return LDAPServer;
    }

    public static void setLDAPServer(String connectionString) {
        LDAPServer = connectionString;
    }
}
