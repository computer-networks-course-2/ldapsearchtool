package GUI;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Dashboard {
    public Label connectionStatusLabel;
    public ListView<String> resultsListView;
    public Label inputErrorLabel;
    public TextField firstNameInput;
    public Button searchBtn;
    public Button exportResultsBtn;
    public Label saveMessage;
    public Label errorLabel;

    private SearchControls searchControls;
    private NamingEnumeration<SearchResult> searchResults;
    private StringBuilder resultsStringBuilder;

    public Dashboard() {
        searchResults = null;
        this.resultsStringBuilder = new StringBuilder();
    }

    private void searchFirstNameInLDAP(String fn) {
        if (!fn.isEmpty()) {
            clearAll();
            try {
                // Filter for searching firstName with existing mail (krbName) and organization unit (ou)
                String filter = "(&(cn=" + fn + " *)(krbName=*)(ou=*)(displayName=*))";

                // Search result
                NamingEnumeration<SearchResult> results =
                        LDAPApplication.LDAPContext.search("", filter, searchControls);

                // Save search results for convert and export persons
                this.searchResults = results;

                if (!results.hasMoreElements()) {
                    errorLabel.setText("No matching results");
                }

                // New arraylist of string for displaying the results
                List<String> list = new ArrayList<>();

                // Iterates through the results
                while (results.hasMoreElements()) {
                    // Gets single element
                    SearchResult result = results.nextElement();

                    // Convert result to single string and add to the arraylist
                    String resultString = result.getAttributes().toString();
                    list.add(resultString);

                    // Get specific attributes of the result
                    Attributes a = result.getAttributes();

                    // Get organizational unit (ou) of result
                    String ouName = a.get("ou").toString();

                    // Get mail attribute (krbName) of the result
                    String mail = a.get("krbName").toString().substring(9);

                    // Only persons with ou=Medical School - Faculty and Staff & existing mail gets added to the
                    // resultsString for export of results.
                    if (ouName.contains("Medical School - Faculty and Staff") && !mail.isEmpty()) {
                        String displayName = a.get("displayName").toString().substring(13);
                        resultsStringBuilder.append(displayName).append(" ").append(mail).append("\n");
                    }
                }

                // Set results to ListView
                this.resultsListView.setItems(FXCollections.observableList(list));

            } catch (NamingException ne) {
                ne.printStackTrace();
            }
        } else {
            // Search field is empty
            this.inputErrorLabel.setText("*Field is required");
        }
    }

    // Clears all error labels and resets search results
    private void clearAll() {
        this.resultsListView.getItems().clear();
        this.searchResults = null;
        this.inputErrorLabel.setText("");
        this.errorLabel.setText("");
        this.saveMessage.setText("");
        this.resultsStringBuilder = new StringBuilder();
    }

    // Export results to .TXT file
    public void exportResultsAndSave() {
        // Check of the resultsStringBuilder is empty
        if (!this.resultsStringBuilder.toString().isEmpty()) {
            FileChooser fs = new FileChooser();

            // Extension file is .txt
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
            fs.getExtensionFilters().add(extFilter);

            //Show save file dialog
            File file = fs.showSaveDialog(null);

            // If file destination, name and resultsStringBuilder are not empty, then export is valid
            if(file != null && !resultsStringBuilder.toString().isEmpty()){
                SaveFile(resultsStringBuilder.toString(), file);
            }
        } else {
            showAlert("There are no results available!");
        }
    }

    // Saving file
    private void SaveFile(String content, File file){
        try {
            // Init of FileWriter
            FileWriter fileWriter = null;

            fileWriter = new FileWriter(file);
            fileWriter.write(content);

            // Close fileWriter
            fileWriter.close();

            saveMessage.setText("Results successfully saved");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    // Show alert dialogue
    private void showAlert(String message) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();
    }

    @FXML
    public void initialize() {
        this.connectionStatusLabel.setText("Connection: " + LDAPApplication.getLDAPServer());

        searchControls = new SearchControls();

        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        searchBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, event ->
                searchFirstNameInLDAP(firstNameInput.getText()));

        firstNameInput.addEventHandler(KeyEvent.KEY_PRESSED, keyEvent -> {
            if (keyEvent.getCode().equals(KeyCode.ENTER)) {
                searchFirstNameInLDAP(firstNameInput.getText());
            }
        });

        exportResultsBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, event ->
                exportResultsAndSave());
    }
}
