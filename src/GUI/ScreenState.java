package GUI;

public enum ScreenState {

    DASHBOARD("Dashboard.fxml"),
    START("Start.fxml");

    private String file;

    ScreenState(String file) {
        this.file = file;
    }

    public String getFile() {
        return "src//GUI//" + file;
    }
}
